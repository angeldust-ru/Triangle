﻿using NUnit.Framework;
using Triangle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Moq;

namespace Triangle.Tests
{
	[TestFixture()]
	public class RightTriangleTests
	{
		[Test()]
		public void GetA_ByDefault_ReturnValue()
		{
			ITriangle triangle = new RightTriangle(3, 4, 5);
			Assert.AreEqual(3,triangle.A);
		}
		[Test()]
		public void GetB_ByDefault_ReturnValue()
		{
			ITriangle triangle = new RightTriangle(3, 4, 5);
			Assert.AreEqual(4,triangle.B);
		}
		[Test()]
		public void GetC_ByDefault_ReturnValue()
		{
			ITriangle triangle = new RightTriangle(3, 4, 5);
			Assert.AreEqual(5,triangle.C);
		}
		
		[Test()]
		public void GetArea_ByDefault_ReturnValue()
		{
			Mock<IAreaComputedStrategy> mock=new Mock<IAreaComputedStrategy>();
			mock.Setup(strategy => strategy.ComputeArea()).Returns(15);
			ITriangle triangle=new RightTriangle(3,4,5) {AreaComputedStrategy = mock.Object};
			Assert.AreEqual(15,triangle.Area);
		}
		[Test()]
		public void GetArea_WhenTrueStrategyAfterFalse_ReturnValue()
		{
			Mock<IAreaComputedStrategy> mock=new Mock<IAreaComputedStrategy>();
			mock.Setup(strategy => strategy.ComputeArea()).Returns(15);
			RightTriangle triangle = CreateRightTrianleWithRecursiveStrategy();
			Assert.IsTrue(triangle.Area < 0);
			triangle.AreaComputedStrategy = mock.Object;
			Assert.AreEqual(15,triangle.Area);
		}
		[Test()]
		public void GetArea_RecursiveStrategy_ReturnNegativeValue()
		{
			RightTriangle triangle = CreateRightTrianleWithRecursiveStrategy();
			Assert.IsTrue(triangle.Area < 0);
		}

		[TestCase(3.0,4.0,5.0,12.0)]
		[TestCase(4.0,3.0,5.0,12.0)]
		[TestCase(9.5,8.4,18.3,36.2)]
		public void GetPerimeter_Default_ReturnPositiveValue(double a,double b,double c,double result)
		{
			ITriangle triangle = new RightTriangle(a,b,c);
			Assert.AreEqual(result,triangle.Perimeter);
		}

		private RightTriangle CreateRightTrianleWithRecursiveStrategy()
		{
			MockComputedAreaWithRecursion strategy = new MockComputedAreaWithRecursion();
			RightTriangle triangle = new RightTriangle(3, 4, 5);
			strategy.Triangle = triangle;
			triangle.AreaComputedStrategy = strategy;
			return triangle;
		}
	}
}