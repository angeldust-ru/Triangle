﻿using NUnit.Framework;
using Triangle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangle.Tests
{
	[TestFixture()]
	public class ComputedAreaByHeronTests
	{
		[Test()]
		public void ComputeAreaTest()
		{
			IAreaComputedStrategy strategy=new ComputedAreaByHeron(CreateTriangle());
			Assert.AreEqual(6,strategy.ComputeArea());
		}

		private ITriangle CreateTriangle()
		{
			return new RightTriangle(3,4,5);
		}


	}
}