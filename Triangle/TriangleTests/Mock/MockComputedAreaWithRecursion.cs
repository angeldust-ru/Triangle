namespace Triangle.Tests
{
	public class MockComputedAreaWithRecursion:IAreaComputedStrategy
	{
		public ITriangle Triangle { get; set; }
		public double ComputeArea()
		{
			return Triangle.Area;
		}
	}
}