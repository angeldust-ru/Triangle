﻿using System;
using System.Threading;

namespace Triangle
{
	public class RightTriangle:GeometricFigure,ITriangle
	{
		private double _perimeter;
		public double A { get; }
		public double B { get; }
		public double C { get; }

		public override double Perimeter
		{
			get
			{
				if (_perimeter < 0)
				{
					_perimeter = A + B + C;
				}
				return _perimeter;
			}
		}

		public RightTriangle(double a, double b, double c)
		{
			A = a;
			B = b;
			C = c;
			_perimeter = -1;
		}
		
	}
}