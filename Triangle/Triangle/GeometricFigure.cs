﻿namespace Triangle
{
	public abstract class GeometricFigure
	{
		private bool _isAreaNoExecute=true;
		private double _area=-1;

		public IAreaComputedStrategy AreaComputedStrategy { get; set; }

		public double Area
		{
			get
			{
				if (_isAreaNoExecute && _area < 0 &&AreaComputedStrategy!=null)
				{
					_isAreaNoExecute = false;
					_area = AreaComputedStrategy.ComputeArea();
					_isAreaNoExecute = true;
				}
				return _area;
			}
		}

		public abstract double Perimeter { get; }
	}
}