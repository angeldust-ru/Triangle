﻿namespace Triangle
{
	public interface IAreaComputedStrategy
	{
		double ComputeArea();
	}
}