﻿using System;

namespace Triangle
{
	public class ComputedAreaByHeron:IAreaComputedStrategy
	{
		private ITriangle _triangle;

		public ComputedAreaByHeron(ITriangle triangle)
		{
			_triangle = triangle;
		}

		public double ComputeArea()
		{
			double p = _triangle.Perimeter/2;
			return Math.Sqrt(p*(p - _triangle.A)*(p - _triangle.B)*(p - _triangle.C));
		}
	}
}