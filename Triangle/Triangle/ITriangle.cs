﻿namespace Triangle
{
	public interface ITriangle
	{
		double A { get; }
		double B { get; }
		double C { get; }

		double Area { get; }
		double Perimeter { get; }
	}
}